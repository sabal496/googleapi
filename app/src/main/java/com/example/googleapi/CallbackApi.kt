package com.example.googleapi

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}